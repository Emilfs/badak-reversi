import json
import logging
from json import JSONDecodeError

from django.http import JsonResponse
from django.shortcuts import render

from app.game_board import AiPlayerInterface, OthelloGameManager


def home(request):
    return render(request, 'home.html')


def apidocs(request):
    return render(request, 'apidocs.html')


def api_next_move(request):
    if request.method == 'POST':
        try:
            # convert API board to engine board
            input_board = json.loads(request.body)
            for row in range(len(input_board)):
                assert len(input_board[row]) == len(input_board)
                for col in range(len(input_board[row])):
                    assert input_board[row][col] == 'rhino' or input_board[row][col] == 'hunter' or input_board[row][col] is None
                    if input_board[row][col] == 'hunter':
                        input_board[row][col] = 1
                    elif input_board[row][col] == 'rhino':
                        input_board[row][col] = 2
                    else:
                        input_board[row][col] = 0

            game = OthelloGameManager(dimension=len(input_board))
            game.board = input_board
            ai = AiPlayerInterface(color=1)
            i, j = ai.get_move(game)
            ai.kill(game)
            game.play(i, j)

            # convert engine board to API board
            output_board = [[None for _ in range(game.dimension)] for _ in range(game.dimension)]
            for row in range(len(game.board)):
                for col in range(len(game.board[row])):
                    if game.board[row][col] == 1:
                        output_board[row][col] = 'hunter'
                    elif game.board[row][col] == 2:
                        output_board[row][col] = 'rhino'

            return JsonResponse(data=output_board, safe=False)
        except JSONDecodeError:
            return JsonResponse(data={'message': 'Problems parsing JSON'}, status=400)
        except Exception as e:
            logging.exception(e)
            return JsonResponse(data={'message': 'Internal Server Error'}, status=500)
    else:
        return JsonResponse(data={'message': 'Not Found'}, status=404)
