#!/usr/bin/env python3
# -*- coding: utf-8 -*

import random
import sys
import time

from app.game_helper import get_score, get_possible_moves, play_move

cached = {}

def compute_utility(board, color):
    """
    Fungsi mereturn utility dari sebuah board state
    (direpresentasikan oleh tuple of tuples) yang dilihat
    dari perspektif setiap peran
    """

    scores = get_score(board)

    if color== 1:
        return scores[0]-scores[1]
    else:
        return scores[1]-scores[0]

############ MINIMAX ###############################
#MAX = 1
#MIN = 2
def minimax_min_node(board, color):
    if color == 1 : opponent = 2
    else : opponent = 1
    if board in cached:
        return cached[board]
    if not get_possible_moves(board,color):
        return compute_utility(board,color)
    v = float("Inf")
    for move in get_possible_moves(board,color):
        v = min(v,minimax_max_node(play_move(board,color,move[0],move[1]),opponent))
    return v
def minimax_max_node(board, color):
    if color == 1 : opponent = 2
    else: opponent = 1
    if board in cached:
        return cached[board]
    if not get_possible_moves(board,color):
        return compute_utility(board,color)
    v = float("-Inf")
    for move in get_possible_moves(board,color):
        v = max(v,minimax_min_node(play_move(board,color,move[0],move[1]),opponent))
    return v

def select_move_minimax(board, color):
    """
    Diberikan sebuah board dan peran player sebagai input,
    tentukan langkah selanjutnya.
    Fungsi mereturn sebuah tuple of integer (i, j),
    dimana i adalah kolom dan j adalah baris pada board.
    """
    moves = []
    for option in get_possible_moves(board,color):
        new_move = play_move(board,color,option[0],option[1])
        utility = minimax_max_node(new_move,color)
        if new_move not in cached:
            cached[new_move] = utility
        moves.append([(option[0], option[1]), utility])
    sorted_options = sorted(moves, key = lambda x: x[1])
    return sorted_options[0][0]

############ ALPHA-BETA PRUNING #####################

def alphabeta_min_node(board, color, alpha, beta, level, limit):
    if color == 1 : opponent = 2
    else: opponent = 1
    level += 1
    if board in cached:
        return cached[board]
    if not get_possible_moves(board,color) or level == limit:
        return compute_utility(board,color)

    v = float("Inf")
    for move in get_possible_moves(board,color):
        new_move = play_move(board, color, move[0], move[1])
        v = min(v,alphabeta_max_node(new_move,opponent,alpha,beta,level,limit))
        if v <= alpha : return v
        beta = min(beta,v)
    return v


def alphabeta_max_node(board, color, alpha, beta, level, limit):
    if color == 1 : opponent = 2
    else: opponent = 1
    level += 1
    if board in cached:
        return cached[board]

    if not get_possible_moves(board,color) or level == limit:
        return compute_utility(board,color)

    v = float("-Inf")
    for move in get_possible_moves(board, color) :
        new_move = play_move(board, color, move[0], move[1])
        v = max(v, alphabeta_min_node(new_move, opponent, alpha, beta, level, limit))
        if v >= beta : return v
        alpha = max(alpha, v)
    return v


def select_move_alphabeta(board, color):
    moves = []
    node_ordering = []
    alpha = float("-Inf")
    beta = float("Inf")

    for option in get_possible_moves(board,color):
        node_ordering.append([(option[0],option[1]),compute_utility(play_move(board,color,option[0],option[1]),color)])
    node_ordering = sorted(node_ordering, key = lambda x : x[1], reverse = True)
    for option in node_ordering:
        new_move = play_move(board, color, option[0][0], option[0][1])
        utility = alphabeta_max_node(new_move, color,alpha,beta, 0, 4)
        if new_move not in cached:
            cached[new_move] = utility
        moves.append([(option[0][0], option[0][1]), utility])
    sorted_options = sorted(moves, key = lambda x : x[1])

    return sorted_options[0][0]


####################################################

def run_ai():
    """
    Fungsi membuat koneksi dengan game manager.
    Pertama-tama, fungsi mendapat perannya dari game manager
    kemudian, fungsi menerima current score dan current board
    state sampai game over dari game manager.
    """
    print("Minimax AI") 
    color = int(input()) 
                         

    while True: 
        next_input = input()
        status, dark_score_s, light_score_s = next_input.strip().split()
        dark_score = int(dark_score_s)
        light_score = int(light_score_s)

        if status == "FINAL": 
            print
        else:
            board = eval(input())
            movei, movej = select_move_alphabeta(board, color)
            print("{} {}".format(movei, movej))


if __name__ == "__main__":
    run_ai()