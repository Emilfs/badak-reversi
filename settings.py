import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


SECRET_KEY = '()guonc9uriv)z*2ortm@)2rstnw-u=!7anxw_nz4z6wep8&+='

DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '[::1]', 'badakreversi.herokuapp.com']


INSTALLED_APPS = [
    'django.contrib.staticfiles',
    'app',
]

MIDDLEWARE = [
    'whitenoise.middleware.WhiteNoiseMiddleware'
]

ROOT_URLCONF = 'urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True
    },
]


STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
