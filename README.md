# Badak Reversi

![build status](https://gitlab.com/Emilfs/badak-reversi/badges/master/pipeline.svg)

A web-based Python implementation of adversarial search for playing the game Reversi.

[View on Heroku](http://badakreversi.herokuapp.com/)

## Running

    $ pip install -r requirements.txt
    $ python manage.py runserver
