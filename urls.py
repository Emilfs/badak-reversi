from django.urls import path

from app import views

urlpatterns = [
    path('', views.home, name='home'),
    path('apidocs', views.apidocs, name='apidocs'),
    path('api/next_move', views.api_next_move, name='api_next_move'),
]
